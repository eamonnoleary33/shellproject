// Author Eamonn O'Leary
// SID 21472336
// I acknowledge DCU academic policies

#include "myshell.h"
#include "utility.c"

int main (int argc, char ** argv)
{
    while (!feof(stdin)) { // Keep running until the end of input is reached 
        if (argc > 1) { // If a BATCH file is provided as an argument, use it as input instead of stdin
            BATCH = 0;
            freopen(argv[1], "r", stdin);
        }
        else {// If no BATCH file is provided, set the BATCH flag to 1
            BATCH = 1;
        }

        if(BATCH == 1) { // If running in BATCH mode, do not print the prompt
            getcwd(cwd, sizeof(cwd)); // Get the current working directory
            (strcat(cwd, "> ")); // Append the prompt symbol to the end of the current working directory string
            fputs (cwd, stdout); // Print the prompt to the command line
        }

        if (fgets (BUF, MAX_BUFFER, stdin )) { // Get the command line input
            arg = args; // Tokenize the input into an array of arguments
            *arg++ = strtok(BUF,SEPARATORS); // Tokenize the input using specified separators
            while ((*arg++ = strtok(NULL,SEPARATORS)));

            if (args[0]) { // Check if a command has been inputted

                if (strcmp(args[0],"quit") == 0) { // checks if it is the quit command sometimes it works sometimes it does not im not sure why to be honest
                    exit(0);
                }
                if (strcmp(args[0],"cd") == 0) { // If the command is "cd", change the current working directory
                    change_directory(args, cwd);
                    continue;
                }

                int j = 0;
                while(args[j] != NULL) { // Check if the command is meant to run in the background
                    if (!strcmp(args[j], "&")) {
                        back = 1; // If the command contains an "&", set the background flag to 1
                    }
                    j++;
                }
                switch(pid = fork()) {// Fork a new process to run the command
                    case -1: // Check for errors in forking
                        printf("Switch error");
                        break;
                    case 0: ;

                        int check = IO(args); // Check for input/output indicators in the command (such as "<", ">", ">>")
                        if(check == 0) {// If no errors were found, execute the command
                            run_command(args, cwd);
                            continue;
                        }
                        else { // If errors were found, display an error message
                            printf("error in IO");
                        }
                        break;
                    default:
                        if(back == 1) { // If running in the background, do not wait for the child process to finish
                            waitpid(pid, &status, WNOHANG);
                            sleep(1);
                            back = 0;
                            continue;
                        }
                        else if (back == 0) { // This is for functions that run in the foreground
                            waitpid(pid, &status, WUNTRACED);                        
                        }
                    }

                }
            }
        }
    
    return 0;
}

void run_command(char **args, char *cwd) {
    if (!strcmp(args[0], "clr")) { // if the first argument is "clr"
        clear_screen(); // call the function to clear the screen
    } else if (!strcmp(args[0], "environ")) { // if the first argument is "environ"
        show_environment(); // call the function to show the environment variables
    } else if (!strcmp(args[0], "dir")) { // if the first argument is "dir"
        list_directory(args); // call the function to list directory contents
    } else if (!strcmp(args[0], "cd")) { // if the first argument is "cd"
        change_directory(args, cwd); // call the function to change the current directory
    } else if (!strcmp(args[0], "pause")) { // if the first argument is "pause"
        pause_program(); // call the function to pause the program
    } else if (!strcmp(args[0], "echo")) { // if the first argument is "echo"
        echo(args); // call the function to echo a string
    } else if (!strcmp(args[0], "help")) { // if the first argument is "help"
        show_help(); // call the function to show the help
    } else if (!strcmp(args[0], "quit")) { // if the first argument is "quit"
        exit(0); // exit the program
    } else { // if the first argument is none of the above
        execute_external_command(args); // call the function to execute an external command
    }
}

void clear_screen() { // function to clear the screen
    execlp("clear", "clear", NULL); // call the clear command
}

void show_environment() { // function to show environment variables
    execlp("env", "env", NULL); // call the env command
}

void list_directory(char **args) { // function to list directory contents
    char *cmd[] = { "ls", "-al", NULL }; // command to list files in long format
    if (args[1]) { // if an argument is given to the dir command
        cmd[1] = args[1]; // change the second argument to the given argument
    }
    execvp(cmd[0], cmd); // execute the command
}

void change_directory(char **args, char *cwd) { // function to change the current directory
    char *dir = args[1]; // get the directory name from the arguments
    if (!dir) { // if no directory is specified
        printf("%s\n", cwd); // print the current directory
        return; // return from the function
    }
    if (chdir(dir) != 0) { // if the directory cannot be changed
        printf("cd: %s: No such file or directory\n", dir); // print an error message
    }
}

void pause_program() { // function to pause the program
    printf("Paused, Press enter to continue"); // print a message
    while (getchar() != '\n') { // loop until the user presses enter
        sleep(1); // sleep for one second
    }
}

void echo(char **args) { // execute echo command
    int i = 1; // start at second arguemnt 
    char string[1024] = ""; // create a blank string
    while(args[i] != NULL) {
        if (strcmp(args[i], ">") == 0 || strcmp(args[i], ">>") == 0 || strcmp(args[i], "<") == 0) { // stops when IO redirection symbol is found
            break;
        }
        else {
            strcat(string, args[i]); // adds word to blank string
            strcat(string, " "); // a space is added to end for next word
            i++;
        }
    }
    execlp("echo", "echo", string, NULL); // string is echod
}

void show_help() { // execute help command
    // Use the execlp() function to execute the 'more' command with the following arguments:
    //   "more"               - the name of the command to execute
    // execlp("more", "more", "-p", "-d", "../manual/readme.md", NULL);
    execlp("more", "more", "../manual/readme.md", NULL);
}

void execute_external_command(char **args) { // execute external commands
    // Use the execvp() function to execute the command with the following arguments:
    //   args[0] - the name of the command to execute
    //   args    - an array of arguments for the command
    execvp(args[0], args);
    // If the command cannot be executed, execvp() returns an error.
}

