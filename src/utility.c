// Author Eamonn O'Leary
// SID 21472336
// I acknowledge DCU academic policies

#include <stdio.h>
#include <string.h>

// Function to handle IO redirection in a command
int IO(char **arg) {
    int j = 0;

    // Loop through all elements of the command
    while (arg[j] != NULL) {
        // If "<" symbol is found, redirect input to file after the symbol
        if (strcmp(arg[j], "<") == 0) {
            // Check if an input file is specified after the "<" symbol
            if (arg[j+1] == NULL) {
                printf("No input file specified.\n");
                return -1;
            }
            // Open the input file and check for errors
            FILE *in_file = fopen(arg[j+1], "r");
            if (in_file == NULL) {
                printf("Error opening input file: %s\n", arg[j+1]);
                return -1;
            }
            // Redirect stdin to the input file
            freopen(arg[j+1], "r", stdin);
            // Set the "<" symbol to NULL to remove it from the command
            arg[j] = NULL;
            j++;
        }
        // If ">" symbol is found, redirect output to file after the symbol
        else if (strcmp(arg[j], ">") == 0) {
            // Check if an output file is specified after the ">" symbol
            if (arg[j+1] == NULL) {
                printf("No output file specified.\n");
                return -1;
            }
            // Open the output file and check for errors
            FILE *out_file = fopen(arg[j+1], "w");
            if (out_file == NULL) {
                printf("Error opening output file: %s\n", arg[j+1]);
                return -1;
            }
            // Redirect stdout to the output file
            freopen(arg[j+1], "w", stdout);
            // Set the ">" symbol to NULL to remove it from the command
            arg[j] = NULL;
            j++;
        }
        // If ">>" symbol is found, redirect output to file after the symbol, appending to existing file
        else if (strcmp(arg[j], ">>") == 0) {
            // Check if an output file is specified after the ">>" symbol
            if (arg[j+1] == NULL) {
                printf("No output file specified.\n");
                return -1;
            }
            // Open the output file and check for errors
            FILE *out_file = fopen(arg[j+1], "a");
            if (out_file == NULL) {
                printf("Error opening output file: %s\n", arg[j+1]);
                return -1;
            }
            // Redirect stdout to the output file, appending to existing content
            freopen(arg[j+1], "a", stdout);
            // Set the ">>" symbol to NULL to remove it from the command
            arg[j] = NULL;
            j++;
        }
        // Increment j to move to the next element of the command
        j++;
    }
    // Return 0 to indicate no errors occurred during IO redirection
    return 0;
}
