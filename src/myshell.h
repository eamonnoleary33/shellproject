// Author Eamonn O'Leary
// SID 21472336
// I acknowledge DCU academic policies

// Libraries
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <signal.h>

#define MAX_BUFFER 1024
#define MAX_ARGS 64
#define SEPARATORS " \t\n"

// external
extern char **environ;

// function prototypes
void run_command(char **args, char *cwd);
void clear_screen();
void show_environment();
void list_directory(char **args);
void change_directory(char **args, char *cwd);
void pause_program();
void echo(char **args);
void show_help();
void execute_external_command(char **args);
int IO(char **args);

// initiating variables
char cwd[255];
char * prompt = cwd;
int BATCH;
char BUF[MAX_BUFFER];
char ** arg;
char * args[MAX_ARGS];
int back = 0;
int status;
int pid;

