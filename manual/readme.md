// Author Eamonn O'Leary
// SID 21472336
// I acknowledge DCU academic policies

NAME 
CA216 SHELL ASSIGNMENT C

DESCRIPTION
myshell is a terminal program written in the C programming language. It accepts commands entered via the prompt or a file and executes them. The shell has a similar look and function to the terminal found in Linux systems. It includes a list of built-in commands, and other commands that i have written from the project spec.

COMMANDS
cd: changing the current directory. Can chnage to a different directory when an arguement is given or if not it will print PWD if no argument is given.
dir: list the contents of directory e.g. ls -al <directory>. Options include returning a list of all files, including hidden files, in the directory using "dir /" or "dir .".
environ: list all environment strings
echo: print what is placed after it onto stdout. Output can be redirected to a file, and echo can accept input redirection and print what is in that file.
help: display the user manual.
clr: clear the screen.
pause: pause operation of the shell until "Enter" is pressed.
quit: quits the shell.

ENVIROMENT
When certain processes are carried out, the system makes use of environment variables. Both the variable names and their qualities are character strings. The system can operate and carry out particular processes in a different way by adjusting the variables of the environment. The environment variables USER (which contains the username), HOME (which contains the path to the home directory), and PATH (which contains a list of directories separated by colons that Linux searches for commands you invoke) are all examples of environment variables.

I/O REDIRECTION
I/O redirection allows us to change where the input and output streams go, which in turn changes where inputs and outputs come from and go to. There are three basic streams: standard input (stdin), standard output (stdout), and standard error (stderr). Output redirection includes two types: "write" (w) and "append" (a). Writing to a file erases all the contents in that file before writing output, while appending adds output to the end of the file. Use ">" to write to a file and ">>" to append to a file. Input redirection replaces the stdin string with a file, and "<" is used to read from a file.

BACKGROUND/FOREGROUND
When a command is called, the base execution process is called foreground execution. The prompt must end with an ampersand (&) in order for a process to run in the background. Because of this, other processes can run in the foreground without affecting the process running in the background. The child running the process is terminated when the background process completes.

REFERENCES
https://www.geeksforgeeks.org/making-linux-shell-c/
https://linuxcommand.org/lc3_lts0070.php